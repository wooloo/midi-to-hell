Converts MIDI files into the [rhythm hell cue format](https://git.wolo.dev/wolo/hell-cue-format).

To make a MIDI file to work with this format...
* Write music in your favorite software
* Make tracks or instruments, silent or audible, that start with `CUE `.
* The name after `CUE ` will be the cue sent to the game.
* Have them play notes when you want the cue to fire.
  * The game will receive the note played as well, so don't place them thoughtlessly.
* All other MIDI features are entirely ignored, so you don't have to worry about any of your other tracks.