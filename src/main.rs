#[macro_use]
extern crate clap;

use std::fs;

use clap::App;

mod convert;
use convert::convert;

fn main() {
	let yaml = load_yaml!("cli.yml");
	let matches = App::from_yaml(yaml).get_matches();
	let in_file = fs::read(match matches.value_of("in") {
		None | Some("-") => "/dev/stdin",
		Some(v) => v,
	})
	.expect("Can't read in file");
	let game = matches.value_of("game").unwrap();
	let hell = convert(game, &in_file);
	let out = match matches.value_of("out") {
		None | Some("-") => "/dev/stdout",
		Some(v) => v,
	};
	fs::write(out, serde_json::to_string(&hell).unwrap().as_bytes()).expect("Couldn't write output")
}
