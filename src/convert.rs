use hell_cue_format::Cue;
use midly::Smf;

/// Converts a MIDI to HellCues
pub fn convert(game: &str, input: &Vec<u8>) -> hell_cue_format::HellCues {
	let smf = Smf::parse(input).expect("Invalid MIDI");
	let mut hell = hell_cue_format::HellCues {
		game_id: game.to_string(),
		cues: vec![],
	};
	for track in smf.tracks {
		let mut timer = 0.0;
		let mut instrument = "None";
		let ticks_per_beat = match smf.header.timing {
			midly::Timing::Metrical(tpb) => tpb.as_int(),
			midly::Timing::Timecode(_, _) => 0,
		};
		let mut ticks_per_second = match smf.header.timing {
			midly::Timing::Metrical(_) => 0.0,
			midly::Timing::Timecode(fps, sub) => 1.0 / fps.as_f32() as f64 / sub as f64,
		};
		let mut bpm = 120;
		for (index, event) in track.iter().enumerate() {
			let delta = if ticks_per_second != 0.0 {
				event.delta.as_int() as f64 / ticks_per_second
			} else {
				let bps = bpm as f64 / 60.0;
				0.666666667 * event.delta.as_int() as f64 / (ticks_per_beat as f64 * bps)
			};
			if !delta.is_nan() && delta.is_finite() {
				timer += delta;
			}
			// eprintln!("{} {}", timer, delta);
			match event.kind {
				midly::TrackEventKind::Meta(message) => match message {
					midly::MetaMessage::Tempo(microseconds_per_beat) => {
						let microseconds_per_tick =
							microseconds_per_beat.as_int() as f64 / ticks_per_beat as f64;
						let seconds_per_tick = microseconds_per_tick / 1000000.0;
						ticks_per_second = 1.0 / seconds_per_tick;
						bpm = (60000000.0 / microseconds_per_beat.as_int() as f64).round() as u32;
						hell.cues.push(Cue {
							time: timer,
							length: 0.0,
							instrument: "bpm".to_string(),
							note: bpm,
						})
					}
					midly::MetaMessage::TrackName(bytes) => {
						instrument = std::str::from_utf8(bytes).unwrap()
					}
					midly::MetaMessage::InstrumentName(bytes) => {
						instrument = std::str::from_utf8(bytes).unwrap()
					}
					other => eprintln!("{:?}", other),
				},
				midly::TrackEventKind::Midi {
					channel: _,
					message,
				} => match message {
					midly::MidiMessage::NoteOn { key, vel: _ } => {
						if instrument.starts_with("CUE ") {
							println!("{}", instrument);
							hell.cues.push(Cue {
								time: timer,
								length: f64::MAX,
								instrument: instrument.chars().skip(4).collect::<String>(),
								note: key.as_int() as u32,
							})
						}
					}

					midly::MidiMessage::NoteOff { key, vel: _ } => {
						if instrument.starts_with("CUE ") {
							match hell
								.cues
								.iter_mut()
								.take(index)
								.filter(|v| {
									v.length == f64::MAX
										&& v.note == key.as_int() as u32 && v.instrument
										== instrument.chars().skip(4).collect::<String>()
								})
								.last()
							{
								Some(v) => v.length = timer - v.time,
								_ => {}
							};
						}
					}
					_ => {}
				},
				_ => {}
			}
		}
	}
	hell
}
